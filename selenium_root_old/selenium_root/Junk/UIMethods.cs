﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using selenium_root.Core;
/*
 * Author:Akash Tyagi
 * Detail: Core Library for UI Specific Controls
 * Date: 25Dec2017
 * History:
 * 
 */


namespace selenium_root.Core
{
	/*
    //Parent Class
	class PageSync
	{
        protected Boolean WaitForElementToAppear(IWebElement element, int TimeOut) { return true; }
        protected Boolean waitForElementAttributeToAppear(IWebElement element, String AttName, String ExpAttValue, int TimeOut){ return true; }
        protected Boolean waitForPageToLoad(int TimeOut){ return true; }
        protected Boolean waitForPageUrlToChange(String old_url, int TimeOut){ return true; }
	}

    //UIEvents Class contains all the all the event based methods
    //Implements Interface: IUIEvents
    class UIEvents:PageSync
    {

		protected void click(IWebElement obj) 
        {
            if (WaitForElementToAppear(obj,Global.TimeOutElementToAppear))
            {
				obj.Click();
				LogAndReport.WriteLogs("info", "Obj Clicked: " + obj.ToString());
            }
            else
            {
				LogAndReport.WriteLogs("fail", "Unable to find the Object. Time Out reached. Unable to click. Obj: " + obj.ToString());
            }




        }
		protected void sendKeys(IWebElement obj, string textToBeSet) 
        {
            obj.SendKeys(textToBeSet);
            LogAndReport.WriteLogs("info", "Value Set: " + textToBeSet + " Object: " + obj.ToString());
        }

		protected string getText(IWebElement obj) 
        {

            string temp = obj.Text;
			LogAndReport.WriteLogs("info", "Text Fetched from object: " + obj.ToString() + " .Text: " + temp );
            return temp;
        }
		protected string getAttributeValue(IWebElement obj, string attName)
		{

			string temp = obj.GetAttribute(attName);
			LogAndReport.WriteLogs("info", "Attribute Value Fetched from object: " + obj.ToString() + " .Att Name: " + attName + " Att value: " + temp);
			return temp;
		}

		protected void selectListByValue(IWebElement obj) { }

		protected void selectListByIndex(IWebElement obj) { }

		protected void rightMouseClick(IWebElement obj, int iX, int iY) { }

		protected void moveMouse(IWebElement obj, int iX, int iY) { }

		protected void dragAndDrop(IWebElement obj, int iX, int iY) { }

	}//end UI events Class

    //THis class Contains methods which are extension to default UI operations
    class UIEventsExtension:UIEvents
    {
		protected string getTableText(IWebElement obj)
		{
			return getText(obj);
		}

		protected void getTableCellTextInRowAndClm(IWebElement obj, int iRow, int iClm)
		{
			//code to written
		}

		protected void getTableRowWithCellTextAs(IWebElement obj) {  }

		protected List<IWebElement> getTableCellElementsInRowAndClm(IWebElement objTable, int iRow, int iClm) { return null; }
    }


    //This class is to define user defined Business methods which are specific to the aplication under test
    class UIUserDefinedAppMethods:UIEventsExtension
    {
        
    }

    //Point of Contact to perform operation on UI. For doing anything on UI create Obj of this Class
    class UIOps:UIUserDefinedAppMethods
    {

        IWebElement element;

        public UIOps()
        {
            
        }

        public UIOps(IWebElement element)
        {
            this.element = element;
        }


    }


*/
}//end name space