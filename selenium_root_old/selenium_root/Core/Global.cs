﻿using System;
using OpenQA.Selenium;

namespace selenium_root.Core
{
    public static class Global
    {
		[ThreadStatic] static IWebDriver _driver;
		[ThreadStatic] static string _tcName;
		[ThreadStatic] static string _tcDescp;
		[ThreadStatic] static string _tcId;
		[ThreadStatic] static string _tcStepName;
        [ThreadStatic] static string _tcStatus;
		[ThreadStatic] static string _tcModuleName;
        [ThreadStatic] static int _timeOutElementToAppear;


		public static IWebDriver GDriver { get { return _driver; } set { _driver = value; } }
        public static string TCName { get{ return _tcName;} set{_tcName = value;} }
		public static string TCDesc { get { return _tcDescp; } set { _tcDescp = value; } }
		public static string TCId { get { return _tcId; } set { _tcId = value; } }
		public static string TCStepName { get { return _tcStepName; } set { _tcStepName = value; } }
		public static string TCStatus { get { return _tcStatus; } set { _tcStatus = value; } }
		public static string TCModuleName { get { return _tcModuleName; } set { _tcModuleName = value; } }
		public static int TimeOutElementToAppear { get { return _timeOutElementToAppear; } set { _timeOutElementToAppear = value; } }





    }
}
