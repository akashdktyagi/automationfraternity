﻿using System;
using System.Diagnostics;
namespace selenium_root.Core
{
    public class LogAndReport
    {
        
        public static void  WriteLogs(string sLogStatus, string sMsg)
        {

            //Variable Declaration
            string ResultLogString;
            string TimeStamp = DateTime.Now.ToString();
            string TCName = Global.TCName;
            string TCId = Global.TCId;
            string StepStatus = sLogStatus;
            string TCDesc = Global.TCDesc;
			string TCStepName = Global.TCStepName;
			string TCModuleName = Global.TCModuleName;

            //Getting the method name where WriteLogs Method is method is called
			Global.TCStepName = Helper.GetMethodName(2);


            ResultLogString = TimeStamp + "|" + StepStatus + "|" + TCStepName + "|" + TCModuleName + "|" + TCId + "|" + TCName + "|" + TCDesc;

            


            //Write in Console For now.  Place Holder to add code to sent this to CSV file and DB
           // Console.WriteLine(ResultLogString);
            Debug.WriteLine(ResultLogString);

        }
    }
}
