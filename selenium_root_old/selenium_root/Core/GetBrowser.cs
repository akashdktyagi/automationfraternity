﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using selenium_root.Core;
namespace selenium_root
{
    public class GetBrowser
    {

        IWebDriver driver;

        //Method Responsible for Getting setting the driver as a Global Variable
        public IWebDriver GetChrome()
        {
            this.driver = new ChromeDriver(Properties.chromeDriverExe);
            Global.GDriver = driver;
            return driver;
        }

    }
}//end namespace

