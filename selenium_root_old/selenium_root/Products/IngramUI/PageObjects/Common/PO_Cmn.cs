﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using selenium_root.Core;

/*
 * Author:Akash Tyagi
 * Created: 10Dec2017
 * Change History:
 * Name     Date       Detail
 * 
 */


namespace selenium_root.Products.IngramUI.PageObjects.Common
{
    public class PO_Cmn : LogAndReport
    {
        //Declare Driver object
        IWebDriver driver;



        //Declare Page properties
        [FindsBy(How = How.Id, Using = "searchBox_Global")]
        private IWebElement searchLink { get; set; }
        [FindsBy(How = How.Id, Using = "search-submit-anchor")]
        private IWebElement searchTextBox { get; set; }

        //Set driver object in constructor
        public PO_Cmn(IWebDriver driverArg)
        {
            this.driver = driverArg;
            //PageFactory.InitElements(driver, this);
            PageFactory.InitElements(this, new RetryingElementLocator(driver, TimeSpan.FromSeconds(20)));

        }

        //****************************************************************
        //***************************Element Level Methdos****************
        //****************************************************************
        private void ClickSearchLink()
        {
            try
            {
                new UIOps(searchLink).oPageSync.WaitForElementToAppear(Global.TimeOutElementToAppear);
                new UIOps(searchLink).oUIEvents.click();
                WriteLogs("info", "Search Link Clicked.");
            }
            catch (Exception e)
            {
                WriteLogs("fail", "Error thrown while trying to Click on Search Link. Error: " + Helper.CatchError(e));
            }

        }

        private void EnterTextBoxInSearchBox(String text)
        {

            try
            {
                searchLink.SendKeys(text);
                
                new UIOps(searchLink).oPageSync.WaitForElementToAppear(Global.TimeOutElementToAppear);
                new UIOps(searchTextBox).oUIEvents.sendKeys(text);
                WriteLogs("info", "Search Box filled with text as:" + text);
            }
            catch (Exception e)
            {
                WriteLogs("fail", "Error thrown while trying to Click on Search Link. Error: " + Helper.CatchError(e));
            }
        }

        private Boolean WaitForSearchResultToAppear()
        {
            //Logic to be written
            return true;

        }

        //****************************************************************
        //***************************KW LEvel Methods*********************
        //****************************************************************
        public void KW_SearchProduct(string prodToBeSearched)
        {
            EnterTextBoxInSearchBox(prodToBeSearched);
            ClickSearchLink();
            if (WaitForSearchResultToAppear())
            {
                WriteLogs("pass", "Product Succesfully Searched:" + prodToBeSearched);
            }
            else
            {
                WriteLogs("fail", "Prodoct Search failed:" + prodToBeSearched);

            }


        }//end method
    }//end class
}//end namspace

