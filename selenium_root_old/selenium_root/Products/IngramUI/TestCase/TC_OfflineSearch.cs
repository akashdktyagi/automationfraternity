﻿using System;
using OpenQA.Selenium;
using selenium_root.Core;
using selenium_root.Products.IngramUI.Resuables;
using selenium_root.Products.IngramUI.PageObjects.Common;


using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;



namespace selenium_root.Products.IngramUI.TestCase
{
    public class TC_OfflineSearch
    {


        public TC_OfflineSearch()
        {
            new GetBrowser().GetChrome();

        }

        public void t_01_VerifyOfflineSearchFunctionality()
        {
            //Product to be Search
            string sProductToBeSearched = "Computer";

            //Navigate to URL
            NavigateToPage.NavigateTo("home");

            PO_Cmn oPOCmn = new PO_Cmn(Global.GDriver);
            oPOCmn.KW_SearchProduct(sProductToBeSearched);




        }

    }//end class
}//end namspace
