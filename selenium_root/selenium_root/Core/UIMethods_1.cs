﻿using System;
namespace selenium_root.Core
{



    //Method List for all the Sync operations
    interface IUISync
    {
 
    //Default Method
    default public boolean waitForElementToAppear(WebElement element, int TimeOut)
        {
            return true;
        }
    
    //Default Method
    default public boolean waitForElementAttributeToAppear(WebElement element, String AttName, String ExpAttValue, int TimeOut)
        {
            return true;
        }
    
    //Default Method
    default public boolean waitForPageToLoad(int TimeOut)
        {
            return true;
        }
    
    //Default Method
    default public boolean waitForPageUrlToChange(String old_url, int TimeOut)
        {
            return true;
        }


    }//end Interface


    interface IUIEventsGeneric extends IUISync
    {
 
    
    //Default Methods in Generic UI events Interface
    default public String getText(WebElement element)
    {
        return element.getText();
    }
    
    default public String getAttribute(WebElement element, String att_name)
    {
        return element.getAttribute(att_name);
    }

    }//end interface

	//Method List for all the Link Specific Operations
	interface IUIEventsLink
	{


	    public void click();

	}//end interface

	//Method List for all the Text Box Specific Operations
	interface IUIEventsTextBox
	{

	    public void sendKeys(String text);


	}//end interface

	//Method List for all the table specific operations
	interface IUIEventsTable
	{

	    public String getTableContent();
	    public int getRowWithCellText(String s_text);

	}//end interface

	//UIMethods Class implements all the UI methods
	class UIMethods implements IUIEventsGeneric, IUIEventsLink, IUIEventsTextBox, IUIEventsTable, IUISync{
	    WebElement element;

	public UIMethods(WebElement obj)
	{
	    this.element = obj;
	}

	public void click()
	{
	    waitForElementToAppear(element, 10);
	    element.click();
	}

	public void sendKeys(String text)
	{
	    element.sendKeys(text);
	}

	public String getTableContent()
	{
	    return getText(element); // Direct use of Default method from IUIEventsGeneric
	}

	public int getRowWithCellText(String s_text)
	{
	    return 0;
	}
	    
	}//End Class

}