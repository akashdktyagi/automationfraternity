﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using selenium_root.Core;
/*
 * Author:Akash Tyagi
 * Detail: Core Library for UI Specific Controls
 * Date: 25Dec2017
 * History:
 * 
 */
namespace selenium_root.Core
{

    //Parent Class
	public class PageSync
	{
        IWebElement element;

        public PageSync(IWebElement element) { this.element = element; }
            

        public Boolean WaitForElementToAppear(int TimeOut) { return true; }
        public Boolean waitForElementAttributeToAppear(String AttName, String ExpAttValue, int TimeOut){ return true; }
        public Boolean waitForPageToLoad(int TimeOut){ return true; }
        public Boolean waitForPageUrlToChange(String old_url, int TimeOut){ return true; }
	}

    //UIEvents Class contains all the all the event based methods
    //Implements Interface: IUIEvents
    public class UIEvents
    {

		IWebElement element;

		public UIEvents(IWebElement element) { this.element = element; }

		public void click() 
        {
  
			element.Click();
			LogAndReport.WriteLogs("info", "Obj Clicked: " + element.ToString());

        }
		public void sendKeys(string textToBeSet) 
        {
            element.SendKeys(textToBeSet);
            LogAndReport.WriteLogs("info", "Value Set: " + textToBeSet + " Object: " + element.ToString());
        }

		public string getText() 
        {

            string temp = element.Text;
			LogAndReport.WriteLogs("info", "Text Fetched from object: " + element.ToString() + " .Text: " + temp );
            return temp;
        }
		public string getAttributeValue(string attName)
		{

			string temp = element.GetAttribute(attName);
			LogAndReport.WriteLogs("info", "Attribute Value Fetched from object: " + element.ToString() + " .Att Name: " + attName + " Att value: " + temp);
			return temp;
		}

		public void selectListByValue() { /*Definition Yet to be Written*/}

		public void selectListByIndex() { /*Definition Yet to be Written*/}

		public void rightMouseClick(int iX, int iY) { /*Definition Yet to be Written*/}

		public void moveMouse(int iX, int iY) { /*Definition Yet to be Written*/}

		public void dragAndDrop(int iX, int iY) { /*Definition Yet to be Written*/}

	}//end UI events Class

    //This class Contains methods which are extension to default UI operations
    public class UIEventsTable
    {
		IWebElement element;

		public UIEventsTable(IWebElement element) { this.element = element; }

		public string getTableText(IWebElement obj){return "";}

        public void getTableCellTextInRowAndClm(IWebElement obj, int iRow, int iClm){}


		public void getTableRowWithCellTextAs(IWebElement obj) { /*Definition Yet to be Written*/ }

		public List<IWebElement> getTableCellElementsInRowAndClm(IWebElement objTable, int iRow, int iClm) { return null; }
    }


    //Point of Contact to perform operation on UI. For doing anything on UI create Obj of this Class
    public class UIOps
    {

        IWebElement element;
        public PageSync oPageSync;
        public UIEvents oUIEvents;
        public UIEventsTable oUITables;


        public UIOps(IWebElement element)
        {
            this.element = element;
			oPageSync = new PageSync(element);
            oUIEvents = new UIEvents(element);
			oUITables = new UIEventsTable(element);

        }




    }



}//end name space