﻿using System;
using System.Reflection;
using System.Diagnostics;
using System.Runtime.CompilerServices;


namespace selenium_root.Core
{
    public  class Helper:LogAndReport
    {

        public static string  GetMethodName(int iLevel){
			StackTrace st = new StackTrace();
			StackFrame sf = st.GetFrame(iLevel);

			return sf.GetMethod().Name;
        }

        public static string CatchError(Exception e)
        {
            //DO something Aftr Error is Caught
            return e.ToString();
        }
    }
}
